package net.ciderpunk.runaway;


import net.ciderpunk.gamebase.gui.GameScreen;
import net.ciderpunk.runaway.gui.RunawayGame;

import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public class Runaway implements ApplicationListener {
	
	private SpriteBatch batch;
	private GameScreen currentScreen;
	private RunawayGame runawayGame;
	private AssetManager assetMan;
	private Boolean loading;
	
	@Override
	public void create() {		
		assetMan = new AssetManager();
		batch = new SpriteBatch();
		runawayGame = new RunawayGame(0,0);
		runawayGame.init(assetMan);
		setScreen(runawayGame);
		loading = true;
	}
	
	public void setScreen(GameScreen newScreen){
		currentScreen = newScreen;
	}
	
	@Override
	public void dispose() {
		assetMan.dispose();
		batch.dispose();
	}

	@Override
	public void render() {	
		if(assetMan.update()) {
			if (loading){	
				currentScreen.postLoad(assetMan);	
				loading = false;
			}
			currentScreen.draw(batch);
			currentScreen.update( Gdx.graphics.getDeltaTime() );
		}	
		else{
			loading = true;
		}
	}

	@Override
	public void resize(int width, int height) {
		runawayGame.resize(width, height);		
	}

	@Override
	public void pause() {
	}

	@Override
	public void resume() {
	}
}

package net.ciderpunk.runaway.gui;


import net.ciderpunk.gamebase.gui.FpsCounter;
import net.ciderpunk.gamebase.gui.GameScreen;
import net.ciderpunk.runaway.ents.Player;
import net.ciderpunk.runaway.ents.Zombie;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public class RunawayGame extends GameScreen {

	public RunawayGame(int w, int h) {
		super(w, h);
		// TODO Auto-generated constructor stub
	}

	Texture men;
	BitmapFont smallFont;
	BitmapFont largeFont;

	
	@Override
	public void update(float dT) {
		// TODO Auto-generated method stub
		super.update(dT);
		
	}

	@Override
	public void draw(SpriteBatch batch) {
		Gdx.gl.glClearColor(0, 0, 1, 0);
		Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT);
		camera.update();
		batch.setProjectionMatrix(camera.combined);
		batch.begin();
		smallFont.draw(batch, "TESTING",  50, 50);
		// TODO Auto-generated method stub
		super.draw(batch);		
		batch.end();
	}

	@Override
	public void init(AssetManager assMan) {
		super.init(assMan);
		assMan.load("fonts/fixedsys.fnt", BitmapFont.class);
		assMan.load("fonts/fixedsys_large.fnt", BitmapFont.class);
		new Player().Load(assMan);
		new Zombie().Load(assMan);
		//Zombie.loadResources(assMan);
		this.addChild(new FpsCounter(this, HorizontalPosition.Right, VerticalPosition.Top, 0,0,"fonts/fixedsys.fnt"));
		super.init(assMan);
		

		
	}

	@Override
	public void postLoad(AssetManager assMan) {
		smallFont = assMan.get("fonts/fixedsys.fnt", BitmapFont.class);
		largeFont = assMan.get("fonts/fixedsys_large.fnt", BitmapFont.class);
		new Player().PostLoad(assMan);
		new Zombie().PostLoad(assMan);
		super.postLoad(assMan);	
		this.addEntity(new Player(200,200,12.0f));
		this.addEntity(new Zombie(100,200,0.0f));
	}
	
	

}

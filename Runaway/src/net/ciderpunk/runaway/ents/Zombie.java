package net.ciderpunk.runaway.ents;

import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.Texture;

import net.ciderpunk.gamebase.ents.Entity;
import net.ciderpunk.gamebase.gfx.Frame;

public class Zombie extends Entity{

	static Frame idle;
	
	public Zombie(){
		super();
	}
	
	public Zombie(int x, int y, float rot) {
		super(x, y, rot);
		this.setVisible(true);
		this.currentFrame = idle;
	}
	
	@Override
	public void update(float dT) {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public void Load(AssetManager assMan) {		
		assMan.load("gfx/men.png", Texture.class);
	}

	@Override
	public void PostLoad(AssetManager assMan) {
		idle = new Frame(assMan.get("gfx/men.png", Texture.class),32,0,32,32,16,16);
	}
	
}

package net.ciderpunk.gamebase.gui;

import java.util.ArrayList;
import java.util.List;

import net.ciderpunk.gamebase.ents.Entity;
import net.ciderpunk.gamebase.gui.GuiElement;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public abstract class GameScreen extends GuiElement {
	
	public GameScreen(int w, int h) {
		super(null, w, h, HorizontalPosition.Left, VerticalPosition.Top, 0, 0 );
	}

	protected OrthographicCamera camera;

	protected List<Entity> entities;
	
	public void init(AssetManager assetMan){
		float w = Gdx.graphics.getWidth();
		float h = Gdx.graphics.getHeight();
		camera = new OrthographicCamera(1, h/w);
		entities = new ArrayList<Entity>();
		super.init(assetMan);
	}
	
	public void addEntity(Entity ent){
		entities.add(ent);
	}

	
	public void resize(int width, int height){
		this.width = width;
		this.height = height;
		if (this.children != null){
			for(GuiElement child : this.children){
				child.updatePosition(width,  height);
			}
		}
		camera.setToOrtho(false, width, height);
	}
	
	public void dispose(){
	}

	public void update(float dT){
		for (Entity ent : this.entities){
			ent.update(dT);
		}
		super.update(dT);
	}
	
	
	public void drawEntities(SpriteBatch batch){
		for (Entity ent : this.entities){
			ent.draw(batch);
		}
	}
	
	public void draw(SpriteBatch batch){
		drawEntities(batch);
		super.draw(batch, 0, 0);
	}
}

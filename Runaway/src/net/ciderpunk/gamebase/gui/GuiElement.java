package net.ciderpunk.gamebase.gui;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public abstract  class GuiElement {

	public enum VerticalPosition{ Top, Bottom, Mid }
	public enum HorizontalPosition{ Left, Right, Mid }

	
	protected VerticalPosition yAnchor;
	protected HorizontalPosition xAnchor;
	protected int width,height;
	protected int xOffs, yOffs;
	protected int xPos, yPos;
	protected List<GuiElement> children = null;
	protected GuiElement parent;
	
	
	public void addChild(GuiElement element){
		if (children == null){
			children = new ArrayList<GuiElement>();
		}
		children.add(element);
	}
	
	public int getWidth() {
		return width;
	}

	public int getHeight() {
		return height;
	}

	public GuiElement getParent() {
		return parent;
	}

	public void init(AssetManager assMan){
		if (children!= null){
			for(GuiElement element : children){
				element.init(assMan);
			}
		}
	}
	
	public void postLoad(AssetManager assMan){
		if (children!= null){
			for(GuiElement element : children){
				element.postLoad(assMan);
			}
		}
	}
	

	public GuiElement(GuiElement parent, int w, int h, HorizontalPosition xAnch, VerticalPosition yAnch, int xOffset, int yOffset){
		this.parent = parent;
		this.width = w; 
		this.height = h;
		this.xAnchor = xAnch;
		this.yAnchor = yAnch;
		this.xOffs = xOffset;
		this.yOffs = yOffset;
	}
	
	public void resize(int width, int height){
		this.width = width;
		this.height = height;
		if (this.children != null){
			for(GuiElement child : this.children){
				child.updatePosition(width,  height);
			}
		}
	}
	
	public void updatePosition(int containerWidth, int containerHeight){
		switch(this.xAnchor){
			case Left:
				this.xPos = this.xOffs;
				break;
			case Right:
				this.xPos = containerWidth - this.xOffs - this.width;;
				break;
			case Mid:
				this.xPos = (containerWidth - this.width) / 2;
				break;
		}
		switch(this.yAnchor){
			case Bottom:
				this.yPos = this.yOffs;
				break;
			case Top:
				this.yPos = containerHeight - this.yOffs - this.height;;
				break;
			case Mid:
				this.yPos = (containerHeight - this.height) / 2;
				break;
		}
	}
	
	public void draw(SpriteBatch batch, int xStart, int yStart){
		drawChildren(batch, xStart + this.xPos, yStart+this.yPos);
	}
	
	public void drawChildren(SpriteBatch batch, int xstart, int ystart){
		if (children != null && !children.isEmpty()){
			Iterator<GuiElement> i = children.iterator();
			while(i.hasNext()){
				i.next().draw(batch, xstart, ystart);
			}
		}
	}
	
	public void update(float dT){
		updateChildren(dT);	
	}
	
	public void updateChildren(float dT){
		if (children != null && !children.isEmpty()){
			Iterator<GuiElement> i = children.iterator();
			while(i.hasNext()){
				i.next().update(dT);
			}
		}
	}
	
}

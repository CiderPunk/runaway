package net.ciderpunk.gamebase.gui;

import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.BitmapFont.TextBounds;
//import com.badlogic.gdx.graphics.g2d.BitmapFont.TextBounds;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.utils.TimeUtils;

public class FpsCounter extends GuiElement {
	String fontName;
	BitmapFont font;
	long nextTime;
	int frames;
	int lastFrames;
	
	public FpsCounter(GuiElement parent, HorizontalPosition xAnch,	VerticalPosition yAnch, int xOffset, int yOffset, String font) {
		super(parent, 50, 50, xAnch, yAnch, xOffset, yOffset);		
		this.fontName = font;
		nextTime = TimeUtils.millis() + 1000;
		frames = 0;
		lastFrames = 0;
	}

	@Override
	public void draw(SpriteBatch batch, int xStart, int yStart) {
		// TODO Auto-generated method stub
		super.draw(batch, xStart, yStart);
		this.font.draw(batch, Integer.toString(lastFrames), xStart + this.xPos, yStart + this.yPos);
		
		frames++;
	}

	@Override
	public void init(AssetManager assMan) {
		assMan.load(this.fontName, BitmapFont.class);
		super.init(assMan);
	}

	@Override
	public void postLoad(AssetManager assMan) {
		font = assMan.get(this.fontName, BitmapFont.class);
		TextBounds bounds = font.getBounds("999FPS");
		this.resize((int)bounds.width, (int)bounds.height);
		this.updatePosition(this.getParent().getWidth(), this.getParent().getHeight());
	}

	@Override
	public void update(float dT) {
		// TODO Auto-generated method stub
		super.update(dT);
		if (TimeUtils.millis() > nextTime){
			lastFrames = frames;
			frames = 0;
			nextTime = TimeUtils.millis() + 1000;
		}
	}
}

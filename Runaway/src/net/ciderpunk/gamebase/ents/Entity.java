package net.ciderpunk.gamebase.ents;



import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import net.ciderpunk.gamebase.gfx.Frame;


public abstract class Entity {

	private int x,y;
	private float rot;
	protected Frame currentFrame;
	
	public Entity(){
		this(0,-0,0.0f);
	}
	
	public Entity(int x, int y, float rot){
		this.x = x;
		this.y = y;
		this.rot = rot;
	}
	

	private boolean active, visible;
	public boolean isActive() {
		return active;
	}
	public void setActive(boolean active) {
		this.active = active;
	}
	public boolean isVisible() {
		return visible;
	}
	public void setVisible(boolean visible) {
		this.visible = visible;
	}

	public int getX() {
		return x;
	}
	public float getRot() {
		return rot;
	}
	public void setRot(float rot) {
		this.rot = rot;
	}
	public void setX(int x) {
		this.x = x;
	}
	public int getY() {
		return y;
	}
	public void setY(int y) {
		this.y = y;
	}
	public Boolean canCollide(){
		return false;
	}
	
	public abstract void update(float dT);
	
	public void draw(SpriteBatch batch){
		if (visible){
			currentFrame.draw(x, y, rot, batch);
		}
	}

	public abstract void Load(AssetManager assMan);

	public abstract void PostLoad(AssetManager assMan);
	
}

package net.ciderpunk.gamebase.gfx;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public class Frame {
	/*
	public enum Position {
		TopLeft,TopCentre,TopRigth,
		MidLeft,Middle,MidRight,
		BtmLeft, BtmCentre, BtmRight,
	}
*/

	private Sprite sprite;
	private int xoffs,yoffs;
	float currentRot;
	
	public Frame(Texture tex, int x, int y, int width, int height, int xOffs, int yOffs){
		sprite = new Sprite(tex,x,y,width,height);
		this.xoffs = xOffs;
		this.yoffs = yOffs;
		sprite.setOrigin(xOffs, yOffs);
		currentRot = 0.0f;
	}
	
	public void draw(int x, int y, SpriteBatch batch){
		draw(x,y, 0.0f, batch);
	}
	
	public void draw(int x, int y, float rot, SpriteBatch batch){
		sprite.setPosition(x-xoffs, y-yoffs);
		sprite.setRotation(rot);
		sprite.draw(batch);
	}
}
